<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct(){
		parent::__construct();
        $this->load->model('m_praktikan','mp');
        $this->load->model('m_asprak','ma');
	}


	public function index()
	{
        $this->load->view('head');
		$this->load->view('login');
    }
    
    public function loginAsprak(){
        $this->load->view('head');
        $this->load->view('login_asprak');
        // $this->input->post('');
    }

    public function loginPraktikan(){
        $this->load->view('head');
        $this->load->view('login_praktikan');
    }

    public function prosesMasukPraktikan(){
        $username = $this->input->post('username', TRUE);
        $password = $this->input->post('password', TRUE);
        
        $cek = $this->mp->cekAkun($username,$password)->row(0,'array');
        $hasil = count($cek);

        if ($cek){
            $object= array(
                'id_praktikan'=>$cek['id_praktikan'],
                'nama_praktikan' =>$cek['nama_praktikan'],
                'pass_praktikan'=>$cek['pass_praktikan']
            );

            $this->session->set_userdata('praktikanLogin',$object);
            // echo print_r($_SESSION);	die;
            
            $this->load->view('head');
            $this->load->view('nilai_praktikan');
        }else{
            print "<script type=\"text/javascript\">alert('Username atau Password anda Salah!');</script>";
            redirect('login/loginPraktikan','refresh');
        }
    }

    public function prosesMasukAsprak(){
        $username = $this->input->post('username', TRUE);
        $password = $this->input->post('password', TRUE);
        
        $cek = $this->ma->cekAkun($username,$password)->row(0,'array');
        $hasil = count($cek);

        if ($cek){
            // $object= array(
            //     'id_praktikan'=>$cek['id_praktikan'],
            //     'pass_praktikan'=>$cek['pass_praktikan']
            // );

            // $this->session->set_userdata('praktikanLogin',$object);
            // echo print_r($_SESSION);	die;

            $data['dataAsprak'] = $this->ma->cekAkunDoang($username, $password);
            $this->load->view('head');
            $this->load->view('index',$data);
        }else{
            print "<script type=\"text/javascript\">alert('Username atau Password anda Salah!');</script>";
            redirect('login/loginAsprak','refresh');
        }
    }
}
