<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komplain extends CI_Controller {
    public function __construct(){
		parent::__construct();
        $this->load->model('m_praktikan','mp');
        $this->load->model('m_asprak','ma');
        $this->load->model('m_komplain','mk');
	}


	public function index()
	{
        $data['dataKomplain'] = $this->mk->ambilKomplain();
        $this->load->view('head');
		$this->load->view('complain_praktikan',$data);
    }

    public function komplainAsprak()
	{
        $data['dataKomplain'] = $this->mk->ambilKomplain();
        $this->load->view('head');
		$this->load->view('complain',$data);
    }
    
    public function ajukanKomplain(){
        $data['dataAsprak'] = $this->ma->ambilAsprak();
        $data['namaPraktikan'] = $this->session->userdata['praktikanLogin']['nama_praktikan'];
		// print_r($data); die;

        $this->load->view('head');
        $this->load->view('ajukan_komplain',$data);
    }

    public function prosesKomplain(){
        $komplain = $this->input->post('komplain');
        $asprak = $this->input->post('asprak');
        $namaPraktikan = $this->input->post('namaPraktikan');

        $this->mk->inputKomplain($komplain,$asprak,$namaPraktikan);
        print "<script type=\"text/javascript\">alert('Sukses Mengajukan Komplain!');</script>";
            redirect('komplain','refresh');
        // echo $komplain."<br>".$asprak;
    }

    public function solusi(){
        $id_komplain = $this->uri->segment(3);
        $data['isi_komplain'] = $this->mk->ambilKomplainAja($id_komplain);
        $this->load->view('head');
        $this->load->view('edit_komplain',$data);
    }


}
?>
