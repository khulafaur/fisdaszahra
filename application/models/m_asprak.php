<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_asprak extends CI_Model {


	public function ambilAsprak()
	{
		$this->db->select('*');
		$this->db->from('asprak');
		// $this->db->where('id_konsumen',$this->session->userdata['sedangLogin']['konsumen_id']);
		return $this->db->get()->result();
		}
		
		public function cekAkun($username,$password)
		{
			$this->db->where('id_asprak',$username);
			$this->db->where('pass_asprak',$password);
			$query = $this->db->get('asprak');
	
			return $query;
			}

			public function cekAkunDoang($username,$password)
			{
				$this->db->where('id_asprak',$username);
				$this->db->where('pass_asprak',$password);
				return $this->db->get('asprak')->row();
			 }
}
?>