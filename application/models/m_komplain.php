<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_komplain extends CI_Model {


	public function inputKomplain($komplain,$asprak,$namaPraktikan)
	{
        $data = array(
            'tgl_pengajuan' 		=> date("Y-m-d H:i:s"),
            'isi_komplain' 	=> $komplain,
            'pengirim' => $namaPraktikan,
            'nama_asprak' => $asprak
        );	

        $this->db->insert('komplain',$data);
    }

    public function ambilKomplain(){
        $this->db->select('*');
		$this->db->from('komplain');
		// $this->db->where('id_konsumen',$this->session->userdata['sedangLogin']['konsumen_id']);
		return $this->db->get()->result();
    }
    public function ambilKomplainAja($id_komplain){
        // $this->db->select('*');
        $this->db->from('komplain');
        $this->db->where('id_komplain',$id_komplain);
		// $this->db->where('id_konsumen',$this->session->userdata['sedangLogin']['konsumen_id']);
		return $this->db->get('asprak')->row();
    }
}
?>