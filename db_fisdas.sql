-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 20, 2019 at 07:48 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_fisdas`
--

-- --------------------------------------------------------

--
-- Table structure for table `asprak`
--

CREATE TABLE `asprak` (
  `id_asprak` int(11) NOT NULL,
  `nama_asprak` varchar(100) NOT NULL,
  `pass_asprak` varchar(100) NOT NULL,
  `telp_asprak` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asprak`
--

INSERT INTO `asprak` (`id_asprak`, `nama_asprak`, `pass_asprak`, `telp_asprak`) VALUES
(1301188549, 'Nicol', '123123', '0978232312'),
(1301188575, 'Novian', '123123', '08267121232'),
(1301188585, 'Khaeruz Zahra', '123123', '082310985097');

-- --------------------------------------------------------

--
-- Table structure for table `komplain`
--

CREATE TABLE `komplain` (
  `id_komplain` int(11) NOT NULL,
  `isi_komplain` varchar(200) NOT NULL,
  `tgl_pengajuan` date NOT NULL,
  `solusi` varchar(200) NOT NULL,
  `nama_asprak` varchar(100) NOT NULL,
  `tgl_selesai` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komplain`
--

INSERT INTO `komplain` (`id_komplain`, `isi_komplain`, `tgl_pengajuan`, `solusi`, `nama_asprak`, `tgl_selesai`) VALUES
(1, 'Hey', '2019-04-20', '', 'Khaeruz Zahra', '0000-00-00'),
(2, 'Coll', '2019-04-20', '', 'Nicol', '0000-00-00'),
(3, 'Nov', '2019-04-20', '', 'Novian', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `mengajar`
--

CREATE TABLE `mengajar` (
  `id_hadir` int(11) NOT NULL,
  `jam_hadir` date NOT NULL,
  `tgl_hadir` date NOT NULL,
  `id_asprak` int(11) NOT NULL,
  `id_praktikan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `modul`
--

CREATE TABLE `modul` (
  `id_modul` int(11) NOT NULL,
  `nama_modul` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `praktikan`
--

CREATE TABLE `praktikan` (
  `id_praktikan` int(11) NOT NULL,
  `nama_praktikan` varchar(100) NOT NULL,
  `pass_praktikan` varchar(100) NOT NULL,
  `nama_klpk` varchar(50) NOT NULL,
  `telp_praktikan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `praktikan`
--

INSERT INTO `praktikan` (`id_praktikan`, `nama_praktikan`, `pass_praktikan`, `nama_klpk`, `telp_praktikan`) VALUES
(1301188582, 'Putri', '123123', 'TT-32', '08229385453');

-- --------------------------------------------------------

--
-- Table structure for table `praktikum`
--

CREATE TABLE `praktikum` (
  `id_praktikum` int(11) NOT NULL,
  `tp` int(11) NOT NULL,
  `ta` int(11) NOT NULL,
  `praktikum` int(11) NOT NULL,
  `jurnal` int(11) NOT NULL,
  `presentasi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asprak`
--
ALTER TABLE `asprak`
  ADD PRIMARY KEY (`id_asprak`);

--
-- Indexes for table `komplain`
--
ALTER TABLE `komplain`
  ADD PRIMARY KEY (`id_komplain`);

--
-- Indexes for table `mengajar`
--
ALTER TABLE `mengajar`
  ADD PRIMARY KEY (`id_hadir`);

--
-- Indexes for table `modul`
--
ALTER TABLE `modul`
  ADD PRIMARY KEY (`id_modul`);

--
-- Indexes for table `praktikan`
--
ALTER TABLE `praktikan`
  ADD PRIMARY KEY (`id_praktikan`);

--
-- Indexes for table `praktikum`
--
ALTER TABLE `praktikum`
  ADD PRIMARY KEY (`id_praktikum`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `komplain`
--
ALTER TABLE `komplain`
  MODIFY `id_komplain` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mengajar`
--
ALTER TABLE `mengajar`
  MODIFY `id_hadir` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `modul`
--
ALTER TABLE `modul`
  MODIFY `id_modul` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `praktikum`
--
ALTER TABLE `praktikum`
  MODIFY `id_praktikum` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
